sleep 1;

if (call TFAR_fnc_haveSWRadio) then {
	[(call TFAR_fnc_activeSwRadio), 1, "130.1"] call TFAR_fnc_SetChannelFrequency; 
	[(call TFAR_fnc_activeSwRadio), 2, "131.1"] call TFAR_fnc_SetChannelFrequency; 
	[(call TFAR_fnc_activeSwRadio), 3, "132.1"] call TFAR_fnc_SetChannelFrequency; 
	[(call TFAR_fnc_activeSwRadio), 4, "133.1"] call TFAR_fnc_SetChannelFrequency; 
	[(call TFAR_fnc_activeSwRadio), 5, "134.1"] call TFAR_fnc_SetChannelFrequency; 
	[(call TFAR_fnc_activeSwRadio), 6, "135.1"] call TFAR_fnc_SetChannelFrequency; 
	[(call TFAR_fnc_activeSwRadio), 7, "136.1"] call TFAR_fnc_SetChannelFrequency; 
	[(call TFAR_fnc_activeSwRadio), 8, "137.1"] call TFAR_fnc_SetChannelFrequency; 
};

if ((call TFAR_fnc_haveLRRadio)) then {
	[(call TFAR_fnc_activeLrRadio), 1, "40.1"] call TFAR_fnc_SetChannelFrequency; 
	[(call TFAR_fnc_activeLrRadio), 2, "41.1"] call TFAR_fnc_SetChannelFrequency; 
	[(call TFAR_fnc_activeLrRadio), 3, "42.1"] call TFAR_fnc_SetChannelFrequency; 
	[(call TFAR_fnc_activeLrRadio), 4, "43.1"] call TFAR_fnc_SetChannelFrequency; 
	[(call TFAR_fnc_activeLrRadio), 5, "44.1"] call TFAR_fnc_SetChannelFrequency; 
	[(call TFAR_fnc_activeLrRadio), 6, "45.1"] call TFAR_fnc_SetChannelFrequency; 
	[(call TFAR_fnc_activeLrRadio), 7, "46.1"] call TFAR_fnc_SetChannelFrequency; 
	[(call TFAR_fnc_activeLrRadio), 8, "47.1"] call TFAR_fnc_SetChannelFrequency; 
	[(call TFAR_fnc_activeLrRadio), 9, "48.1"] call TFAR_fnc_SetChannelFrequency;
};

hint "Radio Frequencies set!";

sleep 5;
hint "";