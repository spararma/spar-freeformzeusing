_parentAction = ["SAFViewDistance","View Distance","",{},{true}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions"], _parentAction] call ace_interact_menu_fnc_addActionToObject;

{
	_action = [format ["SAFViewDistance%1", (str _x)],(str _x),"", compile (format ["setViewDistance %1; setObjectViewDistance %2;", (str _x), (str (_x / 2))]), {true} ] call ace_interact_menu_fnc_createAction;
	[player, 1, ["ACE_SelfActions","SAFViewDistance"], _action] call ace_interact_menu_fnc_addActionToObject;

} forEach [15000, 20000, 24000, 28000, 30000, 35000];

