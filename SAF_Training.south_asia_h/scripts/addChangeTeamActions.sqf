params ['_teams', '_object'];

{ 
	_action = [format ["SPAR_ChangeSquad_Interaction_%1", _x], format ["Join %1", _x],"", compile (format ['[player] joinSilent %1;', _x]) ,{ true }] call ace_interact_menu_fnc_createAction;
	[_object, 0, ['ACE_MainActions'], _action, true] call ace_interact_menu_fnc_addActionToObject;
} forEach _teams;
