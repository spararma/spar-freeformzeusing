params ['_screen', '_cameraSourceObject', '_pipTarget', '_pitch', '_fov'];

_currentCamera = _screen getVariable ['CAMERAOBJECT', objNull];
if (!(isNull _currentCamera)) then {
	deleteVehicle _currentCamera;
	_screen setVariable ['CAMERAOBJECT', objNull];
};

_cameraObject = 'camera' camCreate [0,0,0];
_dir = vectorDir _cameraSourceObject;
_cameraObject setVectorDir (_dir vectorMultiply -1);
_cameraObject cameraEffect ['Internal', 'Back', _pipTarget];
_cameraObject camSetFov _fov;
[_cameraObject, _pitch, 0] call BIS_fnc_setPitchBank;
_cameraObject setPosATL (getPosATL _cameraSourceObject);

_screen setVariable ['CAMERAOBJECT', _cameraObject];

sleep 600;

deleteVehicle _cameraObject;
_screen setVariable ['CAMERAOBJECT', objNull];
