params ['_teleporter'];

_teleportList = [
	['SPAR HQ (Camp Asteria)', 'TELEPORT_HQ'],
	['Airbase', 'TELEPORT_AIRFIELD'],
	['Aircraft Carrier', 'TELEPORT_CARRIER'],
	['Grenade Range (Range 2)', 'TELEPORT_SR2'],
	['Anti-Tank Range (Range 3)', 'TELEPORT_SR3'],
	['Urban Ops Training (Range 4)', 'TELEPORT_SR4'],
	['Vehicle Range (Range 6)', 'TELEPORT_SR6'],
	['Vehicle Testing Course', 'TELEPORT_DRIVINGRANGE'],
	['Vehicle Range (Range 16)', 'TELEPORT_SR16'],
	['Infantry Range (Range 18)', 'TELEPORT_SR18'],
	['Special Operations Command (Camp Themis)', 'TELEPORT_THEMIS'],
	['Marksmanship Range (Range 12)', 'TELEPORT_SR12']
];

_action = ["SPAR_Teleport_Interaction", "Teleport to","", {},{ true }] call ace_interact_menu_fnc_createAction;
[_teleporter, 0, ['ACE_MainActions'], _action, true] call ace_interact_menu_fnc_addActionToObject;

{
	_label = _x select 0;
	_targetName = _x select 1;

	_distanceCheckStatement = compile (format ['(_this distance2D %1) > 250', _targetName]);
	_isFarEnough = _teleporter call _distanceCheckStatement;

	if (isNil '_isFarEnough') then {
		hint _label;
	};

	if (_isFarEnough) then {
		_action = [format ["SPAR_Teleport_Interaction_%1", _targetName], _label,"", compile (format ['player setPosATL (getPosATL %1)', _targetName]) ,{ true }] call ace_interact_menu_fnc_createAction;
		[_teleporter, 0, ['ACE_MainActions', 'SPAR_Teleport_Interaction'], _action, true] call ace_interact_menu_fnc_addActionToObject;
	};
} forEach _teleportList;
