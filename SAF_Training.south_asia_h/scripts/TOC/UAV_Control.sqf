_openTerminalAction = ["SPAR_TOC_OpenUAVTerminal","Control UAV","",{
	[] spawn {
		_currentItem = ((getUnitLoadout player) select 9) select 1;

		if (_currentItem != "B_UavTerminal") then {
			if (_currentItem != "") then {
				player unlinkItem _currentItem;
			};
			player linkItem "B_UavTerminal";
		};

		sleep 0.1;

		player action ["UavTerminalOpen", player];
		waitUntil { sleep 1; isNull (findDisplay 160) };

		player unlinkItem "B_UavTerminal";
		if (_currentItem != "") then  {
			player linkItem _currentItem;
		};
	};
},{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _openTerminalAction, true] call ace_interact_menu_fnc_addActionToObject;


_screens = ["SPAR_TOC_Screens","Screens","",{},{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _screens, true] call ace_interact_menu_fnc_addActionToObject;

_screens_list = [
	['Left', 'TOC_TACSCREEN_L'],
	['Middle', 'TOC_TACSCREEN_M'],
	['Right', 'TOC_TACSCREEN_R']
];

{
	_label = _x select 0;
	_obj = _x select 1;

	
	_screens = [format ["SPAR_TOC_Screens_%1", _label],_label,"",{},{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_TOC_Screens'], _screens, true] call ace_interact_menu_fnc_addActionToObject;

	_uav1Action = ["SPAR_TOC_Screens_UAV1Feed","UAV #1 Feed","", compile (format ["[str (cTabUAVlist select 0),[[1,'rendertarget17']]] remoteExecCall ['cTab_fnc_createUavCam', 0, true]; %1 setObjectTexture [0, '#(argb,512,512,1)r2t(rendertarget17,1)']", _obj]),{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_TOC_Screens', format ["SPAR_TOC_Screens_%1", _label]], _uav1Action, true] call ace_interact_menu_fnc_addActionToObject;

	_uav2Action = ["SPAR_TOC_Screens_UAV2eed","UAV #2 Feed","",compile (format ["[str (cTabUAVlist select 1),[[1,'rendertarget18']]] remoteExecCall ['cTab_fnc_createUavCam', 0, true]; %1 setObjectTextureGlobal [0, '#(argb,512,512,1)r2t(rendertarget18,1)']", _obj]),{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_TOC_Screens', format ["SPAR_TOC_Screens_%1", _label]], _uav2Action, true] call ace_interact_menu_fnc_addActionToObject;

	_defaultScreenAction = ["SPAR_TOC_Screens_Standby","Standby Screen","", compile (format ["%1 setObjectTextureGlobal [0, ""images\standby.jpg""]", _obj]),{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_TOC_Screens', format ["SPAR_TOC_Screens_%1", _label]], _defaultScreenAction, true] call ace_interact_menu_fnc_addActionToObject;

} forEach _screens_list;