params ['_trigger'];

_units = _trigger getVariable 'CREW_MEMBERS';

{
	_x setUnitPos "UP";
} forEach _units;

_shooter = _units select 0;
_marshall1 = _units select 1;
_marshall2 = _units select 2;
_marshall3 = _units select 3;

{
	_x setPosASL (_x getVariable 'CARRIER_ASSIGNED_POSITION');
	_x setDir (180 - (_x getRelDir player));
	if (_x != _shooter) then { [_x, 'Acts_JetsCrewaidF_idle'] remoteExec ['switchMove', 0]; };
} forEach _units;

_veh = vehicle player;

_shooter setVectorDir (vectorDir _trigger);

// // how far is the F/A-18 from the catapult?
_currentPosition = _trigger worldToModel (getPosATL (_veh));
_yDistance = _currentPosition select 1;

if (_yDistance > 3.95) then {
	[_shooter, 'Acts_JetsShooterNavigate_in'] remoteExec ['switchMove', 0];
	[_shooter, 'Acts_JetsShooterNavigate_loop'] remoteExec ['playMoveNow', 0];
	while { _yDistance > 3.95 } do {
		_currentPosition = _trigger worldToModel (getPosATL (_veh));
		_yDistance = _currentPosition select 1;		
		sleep 0.1;
	};

	[_shooter, 'Acts_JetsShooterNavigate_stop'] remoteExec ['switchMove', 0];

	sleep 1;
	[_shooter, 'Acts_JetsShooterNavigate_out_m'] remoteExec ['switchMove', 0];

	sleep 1;
};

if ((_veh isKindOf 'JS_JC_FA18F') || (_veh isKindOf 'JS_JC_FA18E')) then {
	if (((_veh animationPhase 'l_wingfold') == 1) || ((_veh animationPhase 'r_wingfold' == 1))) then {
		hint 'Unfold the wings';

		waitUntil {sleep 0.5; (_veh animationPhase 'l_wingfold') == 0};

		hint '';
	};
};

[_shooter, 'Acts_JetsShooterIdleMoveaway_loop_m'] remoteExec ['playMoveNow', 0];
sleep 4.3;
[_shooter, 'Acts_JetsShooterIdleMoveaway_out_m'] remoteExec ['playMoveNow', 0];

_carrierPart = _trigger getVariable 'CARRIER_PART';
_deflectorAnimName = _trigger getVariable 'DEFLECTOR_ANIM_NAME';

removeAllActions _carrierPart;

if ((_veh isKindOf 'JS_JC_FA18F') || (_veh isKindOf 'JS_JC_FA18E')) then {
	if ((_veh animationPhase 'switch_lbar') != 1) then {
		hint 'Extend the launchbar.';

		waitUntil {sleep 0.5; (_veh animationPhase 'switch_lbar') == 1};

		hint '';
	};

	sleep 2;

	// [_carrierPart,'pos_catapult_01',0] call bis_fnc_carrier01CatapultLockTo;
	[_veh, _carrierPart] call BIS_fnc_attachToRelative;
	hint 'Take back the launchbar.';

	waitUntil {sleep 0.5; (_veh animationPhase 'switch_lbar') == 0};

	hint '';
	sleep 1;
} else {
	[_veh, _carrierPart] call BIS_fnc_attachToRelative;
};

_carrierPart animate [_deflectorAnimName, 10];

sleep 5;

[_shooter, 'Acts_JetsShooterShootingReady_loop_m'] remoteExec ['playMoveNow', 0];

_launchAction = player addAction [
	'LAUNCH',
	{
		params ["_target", "_caller", "_actionId", "_arguments"];
		_trigger = _arguments select 0;

		_trigger setVariable ['LAUNCH', true, true]; 
	},
	[_trigger],
	9,
	true,
	true,
	"",
	"true"
];

waitUntil { sleep 0.25; (_trigger getVariable ['LAUNCH', false]) };
player removeAction _launchAction;

[_marshall1, 'Acts_JetsCrewaidLCrouch_in_m'] remoteExec ['playMoveNow', 0];
[_marshall2, 'Acts_JetsCrewaidRCrouch_in_m'] remoteExec ['playMoveNow', 0];
[_marshall3, 'Acts_JetsCrewaidRCrouch_in_m'] remoteExec ['playMoveNow', 0];

sleep 2;

[_shooter, 'Acts_JetsShooterShootingReady_pointing_m'] remoteExec ['playMoveNow', 0];
sleep 0.5;

[_marshall1, 'Acts_JetsCrewaidLCrouchThumbup_in_m'] remoteExec ['playMoveNow', 0];
[_marshall2, 'Acts_JetsCrewaidRCrouchThumbup_in_m'] remoteExec ['playMoveNow', 0];
[_marshall3, 'Acts_JetsCrewaidRCrouchThumbup_in_m'] remoteExec ['playMoveNow', 0];

sleep 3.7;

[_shooter, 'Acts_JetsShooterShootingLaunch_in_m'] remoteExec ['playMoveNow', 0];
sleep 1.5;

detach _veh;
[_veh] spawn BIS_fnc_AircraftCatapultLaunch;

[_shooter, 'Acts_JetsShooterShootingLaunch_loop_m'] remoteExec ['playMoveNow', 0];
sleep 5;

[_shooter, 'Acts_JetsShooterShootingLaunch_out_m'] remoteExec ['playMoveNow', 0];
sleep 2;

_carrierPart animate [_deflectorAnimName, 0];
