params ['_group', '_trigger', '_carrierPart', '_deflectorAnimName'];

// _trigger setVariable ['CREW_MEMBERS', [], true];

_localPositions = [];

_localPositions pushBack (_trigger modelToWorld [-1.98534,-17.4434,0.1]);
_localPositions pushBack (_trigger modelToWorld [-9.67535,-6.13867,0.1]);
_localPositions pushBack (_trigger modelToWorld [-10.163,1.21094,0.1]);
_localPositions pushBack (_trigger modelToWorld [8.01199,2.23047,0.1]);

_crewMembers = [];

_triggerCarrierPart = nearestObject [_trigger, _carrierPart];
_trigger setVariable ['CARRIER_PART', _triggerCarrierPart, true];
_trigger setVariable ['DEFLECTOR_ANIM_NAME', _deflectorAnimName, true];
_trigger setVariable ['LAUNCH', false, true];

_positionIterator = 0;
{
	_agent = createAgent ["B_Deck_Crew_F", (getPosATL (_x)), [], 0, "CAN_COLLIDE"];
	_agent setDir (getDir (_x));
	_agent setVariable ["BIS_fnc_animalBehaviour_disable", true];

	_agent removeAllEventHandlers "HandleDamage";
	_agent allowDamage false;

	_agent setVariable ['CARRIER_ASSIGNED_POSITION', _localPositions select _positionIterator, true];

	_crewMembers pushBack _agent;
	_positionIterator = _positionIterator + 1;

	deleteVehicle _x;
} forEach units _group;

_trigger setVariable ['CREW_MEMBERS', _crewMembers, true];
deleteGroup _group;